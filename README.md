### docker 实现使用VNC远程linux桌面(xfce4)
```
可以通过app目录预安装app:
    install.sh 安装脚本：
        desktop_link 命令创建桌面应用:
            参数:
                名称  图标  执行命令
            例子:
                desktop_link vscode vscode 'vscode'
                                ^     ^       ^
                               名称   图标    命令
    构建项目:
        ./build.sh
    运行容器:
        docker run -itd -p5901:5901 -e PASSWD="123456" vscode-firefox:latest
        参数说明:
            容器默认监听:5901端口
            容器默认密码: 123456 可以通过 -e 环境变量启动时指定密码
            构建的容器明： vscode-firefox:latest
``` 
### 自定义安装
```
在install 目录下创建安装脚本： 
    脚本支持desktop_link命令和常用的linux脚本命令
    第一行必须以 #import 开头 后面一分号隔开引入的每个安装包， 文件名不支持空格,安装包必须存储在package文件夹内
    内部实现安装命令，命令是在容器内执行，所以需要确保容器有该命令
   

```
##### 桌面截图
![](/desktop.png)

