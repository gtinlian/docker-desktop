### 脚本说明
***
### 第一行必须以 #import 开头
```
cat install-test.sh
~# cat -n install-test.sh
1 #import
2 apt-get install wget
~#

安装wget命令程序，
```
*** 
#### 引入文件 test.zip  test1.zip
```

脚本如下:
~# cat -n install-test.sh
1 #import test.zip test1.zpi
2 tar -xf test.zip && mv test /usr/local/test
3 tar -xf test1.zip && mv test1 /usr/local/test1
~#

第一行引入 test.zip test1.zip
第二，三行操作文件，将程序移动到指定目录

```
<span style="color: red; float: right">注意: 文件第一行必须是 #import</span>
<br />


***
### 安装脚本局限性
``` 
    安装脚本的命令必须是基础容器具备的，如果不具备需要在脚本中安装后才能执行
    脚本支持在build.sh 中使用downloadPackage下载的网络按转包进行安装操作，下载文件在到当前目录
```