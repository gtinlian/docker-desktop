#!/bin/bash

# 构建基础镜像
function buildBase(){
    # 切换到 基础镜像分支，构建基础的xfce容器
    echo '
            *: 默认使用已经构造的基础镜像，基础镜像不存在时必须选择一个基础系统 [1,2]
            1: debian
            2: ubuntu
	 '
    read -p "输入选择的系统编号: " code
    case ${code} in
        1)
            git checkout debian
            docker rmi gtinlian/xfce:base
        ;;
        2)
            git checkout ubuntu
            docker rmi gtinlian/xfce:base
	;;
        *)
            docker image ls xfce:base | grep xfce > /dev/null || \
		   ( echo "基础镜像不存在必须选择一个基础系统 [1,2]" && exit -1 )
        ;;
    esac
    
  
    
    docker image ls gtinlian/xfce:base | grep gtinlian/xfce > /dev/null   || docker build -t gtinlian/xfce:base .
    # 切换到主分支，构建主分支容器
    git checkout master
}
buildBase

# 初始化安装脚本
function appConfigInit(){
    # 创建安装没目录
    if [ ! -d app ]; then
        mkdir app
    fi
    # 安装脚本
    echo '#!/bin/bash

    # 创建桌面图标
    function desktop_link(){
        echo "[Desktop Entry]
        Version=1.0
        Type=Application
        Name=$1
        Comment=
        Exec=$3
        Icon=$2
        Path=
        Terminal=false
        StartupNotify=false" > /root/Desktop/${1}.desktop
    }

    # 如果桌面目录不存在则创建
    if [ ! -d /root/Desktop ];then
        mkdir /root/Desktop
    fi' > app/install.sh
    chmod +x app/install.sh
}
appConfigInit

# 构建镜像
function dockerInstall(){
    # 容器内删除安装目录
    echo 'rm -rf `pwd`' >>  app/install.sh
    if [ $1 != "" ];then
    docker image ls $1 |grep `echo $1 | awk -F ':' '{print $1}'` > /dev/null   ||  docker build -t $1 .
    else
        docker image ls vscode-firefox:latest |grep vscode-firefox > /dev/null   || docker build -t vscode-firefox:latest .
    fi
    # 删除安装目录
    rm -rf app
}

# 处理安装脚本
function addRow(){
    file=$1
    echo "安装: ${file}"
    echo -e "\n" >> app/install.sh
    echo "echo 安装${file}" >> app/install.sh
    for i in `head -1 ${file}`
    do
        if [ "#import" == $i ];then
            continue
        fi
        if [ -f "package/"${i} ];then
            cp -rp "package/"${i}  app
        else
            echo "安装文件不能存在! 安装脚本${file}第一行引入文件: package/$i"
            exit -1
        fi
    done
    row=$[ `cat ${file} | wc -l` + 2 ]
    start=2
    echo -e "\n#script: ${file}" >> app/install.sh
    while [ $[ ${row} - ${start} ] -gt 0 ];do
        sed -n ${start}p  ${file}  >> app/install.sh
        start=$[ ${start} + 1 ]
    done 
}

function packageDownload(){
    cd app
    wget $1
    cd ..
}

packageDownload https://download.jetbrains.com/idea/ideaIU-2021.1.3.tar.gz
addRow install/install-idea.sh

packageDownload http://ftp.mozilla.org/pub/firefox/releases/125.0.1/linux-x86_64/zh-CN/firefox-125.0.1.tar.bz2
addRow install/install-firefox.sh

packageDownload https://download.jetbrains.com/cpp/CLion-2021.1.3.tar.gz
addRow install/install-clion.sh

packageDownload https://download.jetbrains.com/python/pycharm-professional-2021.1.3.tar.gz
addRow install/install-pycharm.sh

packageDownload https://vscode.download.prss.microsoft.com/dbazure/download/stable/e170252f762678dec6ca2cc69aba1570769a5d39/code_1.88.1-1712771838_amd64.deb
addRow install/install-vscode.sh

dockerInstall $1

